#!/bin/bash
primerNombre="Alonso"
primerApellido="Salazar"
fecha=$(date +%Y%m%d_%H%M%S)
usuario="soporte"

echo "Cuál es su nombre?"
read nombre
nombre=$(echo $nombre | tr '[A-Z]' '[a-z]')

if [ $usuario == $nombre ]
then
  echo "Bienvenido ${primerNombre} ${primerApellido}"
else
  echo "Hola $nombre..."
fi
